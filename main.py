import unittest

import HtmlTestRunner
from appium import webdriver

from utils import empty_directory

desired_caps = dict(
    platformName='Android',
    deviceName='Pixel_2_API_29',
    platformVersion='8.1',
    app='/Users/saphirel/PycharmProjects/appium/natais-debug.apk',
    automationName="uiautomator2",
    newCommandTimeou=200
)

# Open the app.
driver = webdriver.Remote('http://0.0.0.0:4723/wd/hub', desired_caps)
driver.implicitly_wait(60)

loader = unittest.TestLoader()
start_dir = "tests"
suite = loader.discover(start_dir)

# Classic test runner
# runner = unittest.TextTestRunner()
empty_directory("reports")
runner = HtmlTestRunner.HTMLTestRunner()
runner.run(suite)
driver.quit()
