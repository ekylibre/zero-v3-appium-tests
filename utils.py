import os

from selenium.webdriver.common.by import By
from selenium.webdriver.support import expected_conditions
from selenium.webdriver.support.wait import WebDriverWait
from time import sleep

from constants import interventions_page_constants, login_page_constants


def login_and_open_crop_selector(driver, operation_id):
    login(driver)
    sleep(5)
    open_crop_selector(driver, operation_id)


def login(driver):
    print("Processing to login with right username/password.")

    el_username_te = driver.find_element_by_id(login_page_constants.USERNAME_TE_ID)
    el_password_te = driver.find_element_by_id(login_page_constants.PASSWORD_TE_ID)
    el_login_btn = driver.find_element_by_id(login_page_constants.SIGN_IN_BTN_ID)
    el_username_te.send_keys(login_page_constants.RIGHT_USERNAME)
    el_password_te.send_keys(login_page_constants.RIGHT_PASSWORD)
    el_login_btn.click()

    WebDriverWait(driver, 20).until(
        expected_conditions.presence_of_element_located(
            (By.ID, interventions_page_constants.REGISTER_INTERVENTION_BTN_ID)))


def open_crop_selector(driver, operation_id):
    sleep(5)
    driver.find_element_by_id(interventions_page_constants.REGISTER_INTERVENTION_BTN_ID).click()
    driver.find_element_by_id(operation_id).click()


def select_crops(driver, n_crops):
    recycler_view = driver.find_element_by_id(interventions_page_constants.SELECT_CROP_RECYCLER_ID)
    crops_by_nature = recycler_view.find_elements_by_class_name("android.view.ViewGroup")

    plots_list = []

    surface = 0
    for e in crops_by_nature:
        e.click()
        plots_list = plots_list + e.find_elements(By.ID, "item_crop_layout")

    if len(plots_list) < n_crops:
        raise Exception

    for i in range(0, len(plots_list)):
        is_checked = plots_list[i].find_element_by_id(interventions_page_constants.CROP_CHECKBOX_ID).get_attribute('checked')
        if n_crops > 0:
            if is_checked == 'false':
                plots_list[i].click()
            n_crops -= 1
            surface += float(plots_list[i].find_element_by_id("crop_area").text.split()[0])
        else:
            if n_crops == 0 and not is_checked == 'false':
                plots_list[i].click()
                surface -= float(plots_list[i].find_element_by_id("crop_area").text.split()[0])

    driver.find_element_by_id(interventions_page_constants.CROPS_VALIDATE_BTN_ID).click()

    return surface


def empty_directory(folder):

    for the_file in os.listdir(folder):
        file_path = os.path.join(folder, the_file)
        try:
            if os.path.isfile(file_path):
                os.unlink(file_path)
            # elif os.path.isdir(file_path): shutil.rmtree(file_path)
        except Exception as e:
            print(e)
