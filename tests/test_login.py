import unittest
from selenium.webdriver.common.by import By
from selenium.webdriver.support import expected_conditions
from selenium.webdriver.support.wait import WebDriverWait

import main
from constants import login_page_constants, interventions_page_constants


class LoginPageTests(unittest.TestCase):

    @classmethod
    def setUp(cls):
        print("-------------------- LOGIN TESTS --------------------")

    @classmethod
    def setUp(cls):
        main.driver.reset()

    def test_username_te(self):
        print("Checking if the username login field is displayed.")
        el = main.driver.find_element_by_id(login_page_constants.USERNAME_TE_ID)
        self.assertIsNotNone(el)

    def test_password_te(self):
        print("Checking if the password login field is displayed.")
        el = main.driver.find_element_by_id(login_page_constants.PASSWORD_TE_ID)
        self.assertIsNotNone(el)

    def test_welcome_text(self):
        print("Checking if the welcome text is displayed and has the right content.")
        el = main.driver.find_element_by_id(login_page_constants.WELCOME_TEXT_ID)
        with self.subTest("Testing field nullability..."):
            print("Testing field nullability...")
            self.assertIsNotNone(el)
        with self.subTest("Testing content..."):
            print("Testing content...")
            self.assertEqual(el.text, login_page_constants.WELCOME_TEXT)

    def test_login_btn_exists(self):
        print("Checking if the password login field is displayed.")
        el = main.driver.find_element_by_id(login_page_constants.SIGN_IN_BTN_ID)
        self.assertIsNotNone(el)

    def test_login_no_data(self):
        print("Processing to login without username/password.")
        el = main.driver.find_element_by_id(login_page_constants.SIGN_IN_BTN_ID)

        snackbar_xpath = "//*[@text='" + login_page_constants.WRONG_USERNAME_OR_PASSWORD + "']"
        el.click()
        WebDriverWait(main.driver, 10).until(expected_conditions.presence_of_element_located((By.XPATH, snackbar_xpath)))
        self.assertIsNotNone(main.driver.find_element(By.XPATH, snackbar_xpath))

    def test_login_wrong_data(self):
        print("Processing to login with wrong username/password.")
        snackbar_xpath = "//*[@text='" + login_page_constants.WRONG_USERNAME_OR_PASSWORD + "']"

        el_username_te = main.driver.find_element_by_id(login_page_constants.USERNAME_TE_ID)
        el_password_te = main.driver.find_element_by_id(login_page_constants.PASSWORD_TE_ID)
        el_login_btn = main.driver.find_element_by_id(login_page_constants.SIGN_IN_BTN_ID)
        el_username_te.send_keys(login_page_constants.WRONG_USERNAME)
        el_password_te.send_keys(login_page_constants.WRONG_USERNAME)
        el_login_btn.click()

        WebDriverWait(main.driver, 10).until(
            expected_conditions.presence_of_element_located((By.XPATH, snackbar_xpath)))
        self.assertIsNotNone(main.driver.find_element(By.XPATH, snackbar_xpath))

    def test_login_right_data(self):
        print("Processing to login with right username/password.")

        el_username_te = main.driver.find_element_by_id(login_page_constants.USERNAME_TE_ID)
        el_password_te = main.driver.find_element_by_id(login_page_constants.PASSWORD_TE_ID)
        el_login_btn = main.driver.find_element_by_id(login_page_constants.SIGN_IN_BTN_ID)
        el_username_te.send_keys(login_page_constants.RIGHT_USERNAME)
        el_password_te.send_keys(login_page_constants.RIGHT_PASSWORD)
        el_login_btn.click()

        WebDriverWait(main.driver, 10).until(
            expected_conditions.presence_of_element_located((By.ID, interventions_page_constants.REGISTER_INTERVENTION_BTN_ID)))
        self.assertIsNotNone(main.driver.find_element(By.ID, interventions_page_constants.REGISTER_INTERVENTION_BTN_ID))
