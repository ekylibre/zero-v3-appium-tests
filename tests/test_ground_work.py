import unittest
from selenium.webdriver.common.by import By

import main
import service
from constants import interventions_page_constants
from utils import select_crops, login_and_open_crop_selector


class GroundWorkPageTests(unittest.TestCase):

    @classmethod
    def setUpClass(cls):
        print("-------------------- GROUND WORK TESTS --------------------")
        main.driver.reset()

    def setUp(self):
        self.service = service.Service()
        super().setUp()

    def test_equipments_selection_edition(self):
        print("Testing ground work intervention creation...")
        login_and_open_crop_selector(main.driver, interventions_page_constants.NEW_GROUND_WORK_BTN_ID)
        select_crops(main.driver, 2)

        main.driver.find_element_by_id(interventions_page_constants.ADD_EQUIPMENT_ZONE_ID).click()

        equipments = main.driver.find_elements_by_class_name("android.view.ViewGroup")
        equipments[1].click()
        main.driver.find_element_by_id(interventions_page_constants.SAVE_INTERVENTION_BTN_ID).click()

        with self.subTest("Action bar"):
            self.assertIsNotNone(main.driver.find_element_by_id(interventions_page_constants.TOOLBAR_ID))

        procedures = main.driver.find_elements(By.ID, interventions_page_constants.ITEM_PROCEDURE_TV_ID)
        self.assertEqual(procedures[0].text, interventions_page_constants.GROUND_WORK)