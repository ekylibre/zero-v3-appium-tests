import unittest
from time import sleep

from selenium.webdriver.common.by import By

import main
from constants import interventions_page_constants
from utils import select_crops, login_and_open_crop_selector


class FertilizationPageTests(unittest.TestCase):

    @classmethod
    def setUpClass(cls):
        print("-------------------- FERTILIZATION TESTS --------------------")
        main.driver.reset()

    def test_fertilization_edition(self):
        print("Testing care intervention creation...")
        login_and_open_crop_selector(main.driver, interventions_page_constants.NEW_FERTILIZATION_BTN_ID)

        select_crops(main.driver, 2)

        main.driver.find_element_by_id(interventions_page_constants.ADD_INPUT_ZONE_ID).click()
        sleep(2)
        inputs = main.driver.find_elements_by_class_name("android.view.ViewGroup")
        inputs[1].click()

        e_t = main.driver.find_element_by_id(interventions_page_constants.INPUT_QUANTITY_EDIT_ID)
        e_t.click()
        e_t.send_keys("12")
        main.driver.execute_script("mobile: performEditorAction", {"action": "done"})

        main.driver.find_element_by_id(interventions_page_constants.SAVE_INTERVENTION_BTN_ID).click()

        procedures = main.driver.find_elements(By.ID, interventions_page_constants.ITEM_PROCEDURE_TV_ID)
        self.assertEqual(procedures[0].text, interventions_page_constants.FERTILIZATION)