import unittest
from time import sleep
from selenium.webdriver.common.by import By

import main
from constants import interventions_page_constants
from utils import select_crops, login_and_open_crop_selector


class ImplantationPageTests(unittest.TestCase):

    @classmethod
    def setUpClass(cls):
        print("-------------------- IMPLANTATION TESTS --------------------")
        main.driver.reset()

    def test_seeds_selection_edition(self):
        print("Testing implantation intervention creation...")
        login_and_open_crop_selector(main.driver, interventions_page_constants.NEW_IMPLANTATION_BTN_ID)
        select_crops(main.driver, 2)

        main.driver.find_element_by_id(interventions_page_constants.ADD_INPUT_ZONE_ID).click()

        seeds = main.driver.find_elements_by_class_name("android.view.ViewGroup")
        seeds[1].click()
        quantity_edit = main.driver.find_element_by_id(interventions_page_constants.INPUT_QUANTITY_EDIT_ID)
        quantity_edit.click()
        quantity_edit.send_keys("12")
        main.driver.execute_script("mobile: performEditorAction", {"action": "done"})
        sleep(1)
        main.driver.find_element_by_id(interventions_page_constants.SAVE_INTERVENTION_BTN_ID).click()

        with self.subTest("Action bar"):
            self.assertIsNotNone(main.driver.find_element_by_id(interventions_page_constants.TOOLBAR_ID))

        procedures = main.driver.find_elements(By.ID, interventions_page_constants.ITEM_PROCEDURE_TV_ID)
        self.assertEqual(procedures[0].text, interventions_page_constants.IMPLANTATION)
