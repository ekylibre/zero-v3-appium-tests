import unittest
from selenium.webdriver.common.by import By

import main
from constants import interventions_page_constants
from utils import select_crops, login_and_open_crop_selector, open_crop_selector


class InterventionsPageTests(unittest.TestCase):

    @classmethod
    def setUpClass(cls):
        print("-------------------- MISC INTERVENTIONS TESTS --------------------")
        main.driver.reset()

    def test_crop_selection_edition(self):
        login_and_open_crop_selector(main.driver, interventions_page_constants.NEW_IMPLANTATION_BTN_ID)
        surface = select_crops(main.driver, 2)

        text = main.driver.find_element_by_id(interventions_page_constants.CROPS_SUMMARY_ID).text.split()
        float_displayed_total = float(text[text.index("ha") - 1])

        self.assertEqual(surface, float_displayed_total)

    def test_register_new_intervention_list(self):
        main.driver.press_keycode(4)
        el = main.driver.find_element_by_id(interventions_page_constants.REGISTER_INTERVENTION_BTN_ID)
        el.click()

        with self.subTest("Implantation button..."):
            self.assertIsNotNone(
                main.driver.find_element_by_id(interventions_page_constants.NEW_IMPLANTATION_BTN_ID))
        with self.subTest("Ground work button..."):
            self.assertIsNotNone(
                main.driver.find_element_by_id(interventions_page_constants.NEW_GROUND_WORK_BTN_ID))
        with self.subTest("Irrigation button..."):
            self.assertIsNotNone(
                main.driver.find_element_by_id(interventions_page_constants.NEW_IRRIGATION_BTN_ID))
        with self.subTest("Harvest button..."):
            self.assertIsNotNone(
                main.driver.find_element_by_id(interventions_page_constants.NEW_HARVEST_BTN_ID))
        with self.subTest("Care button..."):
            self.assertIsNotNone(
                main.driver.find_element_by_id(interventions_page_constants.NEW_CARE_BTN_ID))
        with self.subTest("Fertilization button..."):
            self.assertIsNotNone(
                main.driver.find_element_by_id(interventions_page_constants.NEW_FERTILIZATION_BTN_ID))
        with self.subTest("Crop protection button..."):
            self.assertIsNotNone(
                main.driver.find_element_by_id(interventions_page_constants.NEW_CROP_PROTECTION_BTN_ID))

    def test_surface_calculation(self):
        main.driver.press_keycode(4)
        open_crop_selector(main.driver, interventions_page_constants.NEW_IMPLANTATION_BTN_ID)

        recycler_view = main.driver.find_element_by_id(interventions_page_constants.SELECT_CROP_RECYCLER_ID)
        crops_by_nature = recycler_view.find_elements_by_class_name("android.view.ViewGroup")

        surface = 0
        for e in crops_by_nature:
            e.click()
            a = e.find_elements(By.ID, "item_crop_layout")

            for i in a:
                i.click()
                surface += float(i.find_element_by_id("crop_area").text.split()[0])

        displayed_total = main.driver.find_element_by_id("crop_dialog_total").text.split()
        float_displayed_total = float(displayed_total[displayed_total.index("ha") - 1])
        surface = float('%.1f' % surface) + 0.1

        self.assertEqual(surface, float_displayed_total)
