# Contents

IMPLANTATION = "Implantation"
GROUND_WORK = "Ground work"
IRRIGATION = "Irrigation"
CARE = "Care"
CROP_PROTECTION = "Crop protection"
FERTILIZATION = "Fertilization"
HARVEST = "Harvest"

# IDs

### Misc.
REGISTER_INTERVENTION_BTN_ID = "button_finishing"
TOOLBAR_ID = "action_bar"
SAVE_INTERVENTION_BTN_ID = "button_save"
ITEM_PROCEDURE_TV_ID = "item_procedure"

### New intervention creation buttons
NEW_IMPLANTATION_BTN_ID = "button_implantation"
NEW_GROUND_WORK_BTN_ID = "button_ground_work"
NEW_IRRIGATION_BTN_ID = "button_irrigation"
NEW_HARVEST_BTN_ID = "button_harvest"
NEW_CARE_BTN_ID = "button_care"
NEW_FERTILIZATION_BTN_ID = "button_fertilization"
NEW_CROP_PROTECTION_BTN_ID = "button_crop_protection"

### Choose crops pop up
SELECT_CROP_RECYCLER_ID = "crop_dialog_recycler"
CROPS_VALIDATE_BTN_ID = "button_validate"
CROP_CHECKBOX_ID = "crop_checkbox"
INCLUDE_CROPS_LAYOUT_ID = "include_crops_layout"
CROPS_SUMMARY_ID = "crops_summary"

### Date picker
DATE_LAYOUT_ID = "working_period_layout"
DATE_EDIT_ID = "working_period_edit_date"
DATE_PICKER_MONTH_VIEW_ID = "month_view"

### Input module
ADD_INPUT_ZONE_ID = "input_add_label"
INPUT_QUANTITY_EDIT_ID = "item_quantity_edit"

### Equipment module
ADD_EQUIPMENT_ZONE_ID = "equipment_add_label"

### Material module
ADD_MATERIAL_ZONE_ID = "material_add_label"

### Irrigation module
IRRIGATION_QUANTITY_EDIT_ID = "irrigation_quantity_edit"
IRRIGATION_ARROW_ID = "irrigation_arrow"

### Harvest module
ADD_HARVEST_ZONE_ID = "harvest_add_label"
HARVEST_QUANTITY_EDIT_ID = "harvest_quantity_edit"
